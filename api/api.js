var auth_api = require('./auth_api'),
    provider_api = require('./provider_api'),
    consumer_api = require('./consumer_api'),
    problem_api = require('./problem_api'),
    solution_api = require('./solution_api'),
    bid_api = require('./bid_api'),
    deal_api = require('./deal_api');

var api = {};

api.login = auth_api.login;
api.logout = auth_api.logout;

api.providerAuthentication = auth_api.provider;
api.consumerAuthetnication = auth_api.consumer;
api.logout = auth_api.logout;

api.postProvider = provider_api.post;
api.putProvider = provider_api.put;
api.deleteProvider = provider_api.delete;
api.getProvider = provider_api.get;
api.getOneProvider = provider_api.getOne;
api.getProviderSolution = provider_api.getSolution;
api.getProviderReview = provider_api.getReview;
api.getProviderComment = provider_api.getComment;
api.approveDisapproveComment = provider_api.putComment;

api.postConsumer = consumer_api.post;
api.putConsumer = consumer_api.put;
api.deleteConsumer = consumer_api.delete;
api.getConsumer = consumer_api.get; //fetches all the users in their contact and who viewed
api.getOneConsumer = consumer_api.getOne; // must be a previous customer
api.getConsumerProblem = consumer_api.getProblem;

api.postProblem = problem_api.post;
api.getProblem = problem_api.get;
api.getOneProblem = problem_api.getOne;
api.deleteProblem = problem_api.delete;
api.putProblem = problem_api.put;

api.postSolution = solution_api.post;
api.getSolution = solution_api.get;
api.putSolution = solution_api.put;
api.getOneSolution = solution_api.getOne;
api.deleteSolution = solution_api.delete;
api.makeDeal = solution_api.makeDeal;

api.postBid = bid_api.post;
api.getBid = bid_api.get;
api.getOneBid = bid_api.getOne;
api.acceptRejectBid = bid_api.acceptReject;
api.deleteBid = bid_api.delete;

api.getDeal = deal_api.get;
api.getOneDeal = deal_api.getOne;
api.deleteDeal = deal_api.delete;
api.putDeal = deal_api.put; //only for service providers to trace the time
api.postDealReview = deal_api.postReview;
api.postDealComment = deal_api.postComment;

module.exports = api;