var modules = require('./modules');

var provider_api = {};

provider_api.post = function(req, res) {
    var providerObj = req.body.provider;
    if(modules.isObjValid(providerObj, ["avatar", "total_employees", "registration_number"]) && providerObj.password) {
        modules.Provider.findOne({email: providerObj.email}, function(err, provider) {
            if(err) throw err;
            if(provider) modules.sendError(res, {err: "Duplicate email! Account with this email already exist."}, 400);
            else {
                provider =  new modules.Provider(providerObj);
                provider.save(function(err) {
                    if(err) throw err;
                    provider = provider.toObject();
                    var credential = new modules.Credential({provider: provider._id});
                    modules.bcrypt.genSalt(10, function(err, salt) {
                        if (err) throw err;
                        modules.bcrypt.hash(providerObj.password, salt, null, function(err, hash) {
                            if (err) throw err;
                            credential.password = hash;
                            credential.save(function(err) {
                                if(err) throw err;
                                modules.sendResponse(res, provider);
                            });
                        });
                    });
                });
            }
        });
    } else modules.sendError(res, {err: "Bad request!"}, 400);
};

provider_api.postImage = function(req, res) {
    
};

provider_api.get = function(req, res) {
    modules.Provider.find({}).lean().exec(function(err, providers) {
        if(err) throw err;
        modules.sendResponse(res, providers);
    });
};

provider_api.getOne = function(req, res) {
    var provider_id = +req.params.provider_id;
    if(req.provider) {
        if(req.provider._id === provider_id) modules.Provider.findById(provider_id).lean().exec(function(err, provider) {
            if(err) throw err;
            if(provider) modules.sendResponse(res, provider);
            else modules.sendError(res, {err: "provider not found"}, 404);
        }); else modules.sendError(res, {err: "Not allowed"}, 405);
    } else modules.sendError(res, {err: "Bad request"}, 400);
};

provider_api.put = function(req, res) {
    var provider_id = +req.params.provider_id, providerData = req.body.provider, valid = false;
    for(var key in providerData) {
        if(providerData[key]) valid = true;
    }
    if(req.provider && valid) {
        if(req.provider._id === provider_id) modules.Provider.findById(provider_id).exec(function(err, provider) {
            if(err) throw err;
            if(provider) {
                for(var key in providerData) {
                    if(providerData[key]) provider[key] = providerData[key];
                }
                provider.save(function(err) {
                    if(err) throw err;
                    modules.sendResponse(res, provider.toObject());
                });
            } else modules.sendError(res, {err: "provider not found"}, 404);
        }); else modules.sendError(res, {err: "Not allowed"}, 405);
    } else modules.sendError(res, {err: "Bad request"}, 400);
};

provider_api.delete = function(req, res) {
    var provider_id = +req.params.provider_id;
    if(req.provider) {
        if(req.provider._id !== provider_id) provider_id = undefined;
    } else provider_id = undefined;
    if(provider_id) modules.Provider.remove({_id: provider_id}, function(err) {
        if(err) throw err;
        modules.sendResponse(res, {status: 200});
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

provider_api.getSolution = function(req, res) {
    var provider_id = +req.params.provider_id;
    if(req.provider._id === provider_id) modules.Solution.find({provider: provider_id}).lean().exec(function(err, solutions) {
        if(err) throw err;
        modules.sendResponse(res, solutions);
    }); else modules.sendError(res, {err: "Not allowed"}, 405);
};

provider_api.getReview = function(req, res) {
    var provider_id = +req.params.provider_id;
    
    if(provider_id) modules.Deal.find({provider: provider_id, status: "ready"}).lean().exec(function(err, deals) {
        if(err) throw err;
        var async_funcs = [];
        deals.forEach(function(deal) {
            async_funcs.push(function(cb) {
                modules.DealReview.findOne({deal: deal._id}).populate({path:"consumer", select:"_id first_name last_name email avatar"}).lean().exec(function(err, review){
                    if(err) throw err;
                    deal.review = review;
                    cb();
                });
            }); 
        });
        if(async_funcs.length) modules.async.parallel(async_funcs, function(err) {
            if(err) throw err;
            modules.sendResponse(res, deals);
        }); else modules.sendResponse(res, deals);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

provider_api.getComment = function(req, res) {
    var provider_id = +req.params.provider_id;
    if(provider_id) modules.Deal.find({provider: provider_id, status: "ready"}).lean().exec(function(err, deals) {
        if(err) throw err;
        var async_funcs = [];
        deals.forEach(function(deal) {
            async_funcs.push(function(cb) {
                var query;
                if(req.provider) {
                    if(req.provider._id === provider_id) query = {deal: deal._id}
                } else query = {deal: deal._id, status: "approved"};
                modules.DealComment.findOne(query).populate({path:"consumer", select:"_id first_name last_name email avatar"}).lean().exec(function(err, comment){
                    if(err) throw err;
                    deal.comment = comment;
                    cb();
                });
            }); 
        });
        if(async_funcs.length) modules.async.parallel(async_funcs, function(err) {
            if(err) throw err;
            modules.sendResponse(res, deals);
        }); else modules.sendResponse(res, deals);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

provider_api.putComment = function(req, res) {
    var provider_id = +req.params.provider_id, review_id = +req.query.review_id, status = (req.query.status === "approved" || req.query.status === "disapproved") ? req.query.status : undefined;
    if(req.provider && review_id && provider_id && status) {
        if(req.provider._id === provider_id) modules.DealComment.findById(review_id, function(err, comment) {
            if(err) throw err;
            modules.Deal.findOne({_id: comment.deal, provider: provider_id}).lean().exec(function(err, deal) {
                if(err) throw err;
                if(deal) {
                    comment.status = status;
                    comment.save(function(err) {
                        if(err) throw err;
                        modules.sendResponse(res, comment.toObject());
                    });
                } else modules.sendError(res, {err: "Not allowed"}, 405);
            });
        }); else modules.sendError(res, {err: "Not allowed"}, 405);
    } else modules.sendError(res, {err: "Bad request"}, 400);
};

module.exports = provider_api;