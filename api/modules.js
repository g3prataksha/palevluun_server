function Modules() {
    this.async = require('async');
    this.sanitizeHtml = require('sanitize-html');
    this.validator = require('validator');
    this.bcrypt = require('bcrypt-nodejs');
    this.jwt = require('jsonwebtoken');
    this.fs = require('fs');
    this.uuid = require('uuid')
    this.morgan = require('morgan');
    this.multer = require('multer');
    //models
    this.Subscriber = require('./models/Subscriber');
    this.Contact = require('./models/Contact');
    this.Provider = require('./models/Provider');
    this.Consumer = require('./models/Consumer');
    this.Credential = require('./models/Credential');
    this.CompanyReview = require('./models/CompanyReview');
    this.CompanyComment = require('./models/CompanyComment');
    this.Problem = require('./models/Problem');
    this.Solution = require('./models/Solution');
    this.DealReview = require('./models/DealReview');
    this.DealComment = require('./models/DealComment');
    this.Bid = require('./models/Bid');
    this.Deal = require('./models/Deal');
    
    this.objectIdRegex = /^[0-9a-fA-F]{24}$/;
    this.secret = "Palveluun services";
    this.contentType = {'content-type': 'application/json; charset=utf-8'};
    this.transporter = require('nodemailer').createTransport({
        host: 'posti.zoner.fi',
        //port: 587,
        secure: true, // use SSL
        auth: {
            user: 'no-reply@shifter.fi',
            pass: 'Billy778'
        }
    });
}

Modules.prototype.isObjValid = function(obj, exception_keys) {
    var valid = true; exception_keys = Array.isArray(exception_keys) ? exception_keys : [];
    if(obj) for(var key in obj) {
        if(!obj[key] && (exception_keys.indexOf(key) < 0)) {
            console.log("invlaid key:", key);
            valid = false;
        }
    } else valid = false;
    return valid;
};

Modules.prototype.sendResponse = function(res, json_data) {
    //res.set(this.contentType);
    res.json(json_data);
};

Modules.prototype.sendError = function(res, json_error, status) {
    res.set(this.contentType);
    res.status(status).json(json_error);
};

Modules.prototype.sendImage = function(res, image) {
    if(image.key) this.s3.getObject({Bucket: this.bucket, Key: image.key}).createReadStream().pipe(res);
    else res.status(400).json({err: "Bad request!"});
};

module.exports = new Modules();