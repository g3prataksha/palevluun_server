var modules = require('./modules');

var bid_api = {};

//only for provider
bid_api.post = function(req, res) {
    if(modules.isObjValid(req.body.bid) && req.provider) modules.Problem.findById(req.body.bid.problem).lean().exec(function(err, problem) {
        if(err) throw err;
        if(problem) {
            modules.Bid.count({problem: problem._id, provider: req.provider._id}, function(err, count) {
                if(err) throw err;
                if(!count) {
                    var bid = new modules.Bid({problem: req.body.bid.problem, provider: req.provider._id, last_date: req.body.bid.last_date});
                    bid.save(function(err) {
                        if(err) throw err;
                        modules.sendResponse(res, bid.toObject());
                    });
                } else modules.sendError(res, {err: "Bid already exists. Please cancel/delete the old bid to send a new one"}, 405);
            });
        } else modules.sendError(res, {err: "Problem not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

bid_api.get = function(req, res) {
    var query, populate, status = req.query.status;
    if(req.consumer) {
        query = {status: status};
        //populate = [{path: "provider", select: "name avatar"}, {path: "problem", select: "_id", match:{consumer: req.consumer._id}}];
    }
    else if(req.provider) {
        query = {provider: req.provider._id, status: status};
        //populate = [{path: "provider", select: "name avatar"}];
    }
    if(query && status) modules.Bid.find(query).sort({_id: -1}).populate([{path: "provider", select: "name avatar", options: {lean: true}}, {path: "problem", options: {lean: true}}]).lean().exec(function(err, bids) {
        if(err) throw err;
        var json = [];
        if(req.consumer) bids.forEach(function(bid, i) {
            modules.Problem.findOne({_id: bid.problem, consumer: req.consumer._id}).lean().exec(function(err, problem) {
                if(err) throw err;
                if(problem) json.push(bid);
                if(i === (bids.length-1)) modules.sendResponse(res, json);
            });
        }); else if(req.provider) modules.sendResponse(res, bids);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

bid_api.getOne = function(req, res) {
    var bid_id = +req.params.bid_id, query;
    if(req.consumer) query = {_id: bid_id};
    else if(req.provider) query = {provider: req.provider._id, _id: bid_id};
    
    if(bid_id && query) modules.Bid.findOne(query).populate([{path: "provider", select: "name avatar email contact_person phone"}, {path: "problem", options: {lean: true}}]).lean().exec(function(err, bid) {
        if(err) throw err;
        if(bid) {
            if(req.consumer) modules.Problem.findOne({_id: bid.problem, consumer: req.consumer._id}).lean().exec(function(err, problem) {
                if(err) throw err;
                if(problem) modules.sendResponse(res, bid);
            }); else if(req.provider) modules.sendResponse(res, bid);
        } else modules.sendError(res, {err: "Bid not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

//only for consumer
bid_api.acceptReject = function(req, res) {
    var bid_id = +req.params.bid_id, status = (req.query.status === "accept" || req.query.status === "reject") ? req.query.status : undefined;
    if(req.consumer && status && bid_id) modules.Bid.findOne({_id: bid_id, status: "pending", last_date: {$gt: new Date()}}).exec(function(err, bid) {
        if(err) throw err;
        if(bid) {
            modules.Problem.findOne({_id: bid.problem, consumer: req.consumer._id}, function(err, problem) {
                if(err) throw err;
                if(problem) {
                    modules.Deal.findOne({problem: problem._id, consumer: req.consumer._id}).lean().exec(function(err, deal) {
                        if(err) throw err;
                        if(deal) modules.sendError(res, {err: "Deal for this problem is already made."}, 405);
                        else {
                            bid.status = status+"ed";
                            bid.save(function(err) {
                                if(err) throw err;
                                //if accepted create new deal
                                problem.status = "closed";
                                problem.save(function(err) {
                                    if(err) throw err;
                                    deal = new modules.Deal({bid: bid._id, problem: problem._id, consumer: req.consumer._id, provider: bid.provider, created_by: "consumer", uuid: modules.uuid.v4()})
                                    deal.save(function(err) {
                                        if(err) throw err;
                                        modules.sendResponse(res, deal.toObject());
                                    });
                                });
                            });
                        }
                    });
                } else modules.sendError(res, {err: "Problem not found"}, 404);
            });
        } else modules.sendError(res, {err: "Bid not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

bid_api.delete = function(req, res) {
    var bid_id = +req.params.bid_id;
    if(req.provider && bid_id) modules.Bid.remove({_id: bid_id, provider: req.provider._id, status: "pending"}).exec(function(err) {
        if(err) throw err;
        modules.sendResponse(res, {status: 200});
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

module.exports = bid_api;