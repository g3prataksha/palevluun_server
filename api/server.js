
var http = require('http');
var path = require('path');
var fs = require('fs');
//var socketio = require('socket.io');
var express = require('express');
var cors = require('cors');
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connect('localhost/palveluunDB'));
mongoose.Promise = global.Promise;
var bodyParser = require('body-parser');

var modules = require('./modules');
var api = require('./api');

var router = express();
var http_server = require('http').Server(router);

var basicAuth = require('basic-auth');

var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.status(401).send();
  };

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  };

  if (user.name === 'palveluun' && user.pass === 'services') {
    return next();
  } else {
    return unauthorized(res);
  }
};

var uploader = modules.multer({
  fileFilter: function(req, file, cb) {
    if (file.mimetype.indexOf('image/jpeg') > -1 || file.mimetype.indexOf('image/png') > -1 || file.mimetype.indexOf('image/gif') > -1) {
      if (file.size > 5000000) {
        console.log('image file is bigger!');
        cb(null, false);
      }
      else cb(null, true);
    }
    else {
      console.log('incorrect file format');
      cb(null, false);
    }
  },
  storage: modules.multer.diskStorage({
      filename: function (req, file, cb) {
        var filename = file.originalname.replace(/'.'/g, '-') + '-' + modules.generateUniqueId();
        var extension = file.mimetype.replace('image/', '');
        filename = filename + '.' + extension;
        console.log(filename + ' uploaded!');
        cb(null, filename);
      },
      destination: function (req, file, cb) {
        cb(null, __dirname + '/uploads')
      }
  })
}).single('image');

router.post('/subscribe', function(req, res) {
  var emailValid = req.body ? modules.validator.isEmail(req.body.email) : undefined;
  if(emailValid) modules.Subscriber.findOne({email: req.body.email.toLowerCase()}).lean().exec(function(err, subscriber) {
    if(err) throw err;
    if(subscriber) res.json(subscriber);
    else {
      subscriber = modules.Subscriber({email: req.body.email.toLowerCase()});
      subscriber.save(function(err) {
        if(err) throw err;
        res.json(subscriber.toObject());
      });
    }
  }); else res.status(400).end("Invalid email!");
});

/*
var io = require('socket.io')(http_server); 

io.use(function(socket, next) {
  var handshakeData = socket.request;
  socket.token = handshakeData._query['token'];
  next();
});

io.on('connection', function(socket) {
	if(socket.token) modules.jwt.verify(socket.token, modules.secret, function(err, decoded) {
    if (err) {
      socket.disconnect();
      console.log('socket failed to connect', err);
    } else if (decoded) {
      if(parseInt(decoded.user_id)) modules.User.findById(decoded.user_id).lean().exec(function(err, user) {
        if (err) throw err;
        if (user) {
          socket.user_id = user._id;
          modules.sockets.push(socket);
          socket.emit('authenticated', {id: socket.id});
          console.log('socket successfully connected', socket.id);
        } else {
          console.log('socket failed to connect', socket.id);
          socket.disconnect();
        }
      }); else {
        console.log('socket failed to connect', socket.id);
        socket.disconnect();
      }
    } else {
      console.log('socket failed to connect, bad token', socket.id);
      socket.disconnect();
    }
  }); else {
    console.log('socket failed to connect, no token', socket.id);
    socket.disconnect();
  }
	
  socket.on('disconnect', function() {
    var index = modules.sockets.indexOf(socket);
    if(index > -1) modules.sockets.splice(index, 1);
    console.log("socket disconnected", socket.id);
  });
});
*/

router.use(cors())
router.use(bodyParser.json()); // to support JSON-encoded bodies
router.use(bodyParser.urlencoded({
  extended: true
})); // to support URL-encoded bodies
// use morgan to log requests to the console
router.use(modules.morgan('dev'));
router.use(require('cookie-parser')('Palveluun services'));

router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.query.token || req.body.token || req.signedCookies.token || req.headers['x-access-token'];
  
  req.socket_id = req.body.socket_id || req.query.socket_id || req.headers['socket-id'] || undefined;

  req.query.last_id = parseInt(req.query.last_id) ? parseInt(req.query.last_id) : undefined;

  if (req.body.login) req.body.login = {
    email: modules.validator.isEmail(req.body.login.email) ? req.body.login.email.toLowerCase() : undefined,
    password: req.body.login.password
  };
  
  if(req.body.consumer) req.body.consumer = {
    first_name: req.body.consumer.first_name, last_name: req.body.consumer.last_name, email: modules.validator.isEmail(req.body.consumer.email) ? req.body.consumer.email.toLowerCase() : undefined,
    phone: req.body.consumer.phone, country: req.body.consumer.country, city: req.body.consumer.city, postal: req.body.consumer.postal, address: req.body.consumer.address,
    preferable_contact_media: req.body.consumer.preferable_contact_media, preferable_time: req.body.consumer.preferable_time,
    password: req.body.consumer.password === req.body.consumer.confirm_password ? req.body.consumer.confirm_password : undefined
  };
  
  if(req.body.provider) req.body.provider = {
    name: req.body.provider.name, registration_number: req.body.provider.registration_number, 
    email: modules.validator.isEmail(req.body.provider.email) ? req.body.provider.email.toLowerCase() : undefined,
    phone: req.body.provider.phone, country: req.body.provider.country, city: req.body.provider.city, postal: req.body.provider.postal, address: req.body.provider.address,
    preferable_contact_media: req.body.provider.preferable_contact_media, preferable_time: req.body.provider.preferable_time,
    total_employees: parseInt(req.body.provider.total_employees) ? parseInt(req.body.provider.total_employees) : undefined,
    sector: parseInt(req.body.provider.sector) ? parseInt(req.body.provider.sector) : undefined,
    sub_sector: parseInt(req.body.provider.sub_sector) ? parseInt(req.body.provider.sub_sector) :undefined,
    password: req.body.provider.password === req.body.provider.confirm_password ? req.body.provider.confirm_password : undefined
  };
  
  if(req.body.bid) req.body.bid = {
    problem: +req.body.bid.problem,
    price_range: +req.body.bid.price_range.start && +req.body.bid.price_range.end && req.body.bid.price_range.currency ? {
      start: +req.body.bid.price_range.start, end: +req.body.bid.price_range.end, currency: modules.sanitizeHtml(req.body.bid.price_range.currency)
    } : undefined,
    last_date: +req.body.bid.last_date
  };
  
  if(req.body.solution) req.body.solution = {
    title: req.body.solution.title,
    description: req.body.solution.description,
    special_offer: req.body.solution.special_offer,
    sector: +req.body.solution.sector,
    subSector: +req.body.solution.subSector,
    price_range: +req.body.solution.price_range.start && +req.body.solution.price_range.end && req.body.solution.price_range.currency ? {
      start: +req.body.solution.price_range.start, end: +req.body.solution.price_range.end, currency: modules.sanitizeHtml(req.body.solution.price_range.currency)
    } : undefined,
    last_date: +req.body.solution.last_date,
    type: req.body.solution.type === "top_offer" ? "top_offer" : undefined
  };
  
  if(req.body.solution) {
    if(!Array.isArray(req.body.solution.title)) req.body.solution.title = undefined;
    if(!Array.isArray(req.body.solution.description)) req.body.solution.description = undefined;
    if(!Array.isArray(req.body.solution.special_offer)) req.body.solution.special_offer = undefined;
  }
  
  if(req.body.problem) req.body.problem = {
    prefered_location: req.body.problem.prefered_location ? modules.sanitizeHtml(req.body.problem.prefered_location) : undefined,
    brand: req.body.problem.brand ? modules.sanitizeHtml(req.body.problem.brand) : undefined,
    equipment_info: req.body.problem.equipment_info ? modules.sanitizeHtml(req.body.problem.equipment_info) : undefined,
    last_date: +req.body.problem.last_date,
    price_range: +req.body.problem.price_range.start && +req.body.problem.price_range.end && req.body.problem.price_range.currency ? {
      start: +req.body.problem.price_range.start, end: +req.body.problem.price_range.end, currency: modules.sanitizeHtml(req.body.problem.price_range.currency)
    } :undefined,
    title: req.body.problem.title,
    description: req.body.problem.description,
    sector: +req.body.problem.sector,
    subSector: +req.body.problem.subSector
  };
  
  if(req.body.problem) {
    if(!Array.isArray(req.body.problem.title)) req.body.problem.title = undefined;
    if(!Array.isArray(req.body.problem.description)) req.body.problem.description = undefined;
  }
 
  //bypass this path
  if (req.method.toUpperCase() === "POST" && (req.url === "/login" || req.url === "/consumer" || req.url === "/provider")) next();
  else if (req.method.toUpperCase() === "PUT" && (req.url === "/reset")) next();
  else if (req.url === "/contact" || req.url === "/subscribe") next();
  else if (token) { // decode token
 
    // verifies secret and checks exp
    modules.jwt.verify(token, modules.secret, function(err, decoded) {
      if (err) {
        console.log("Err ->", err, '| route ->', req.url, ' | token given ->', token);
        return res.status(403).json({
          err: 'Failed to authenticate token.'
        });
      }
      else if (decoded) {
        // if everything is good, save to request for use in other routes
        console.log("decoded:", JSON.stringify(decoded));
        if(modules.validator.isEmail(decoded.email)) modules.Provider.findOne({email: decoded.email}).lean().exec(function(err, provider) {
          if (err) throw err;
          if (provider) {
            req.provider = provider;
            next();
          } else modules.Consumer.findOne({email: decoded.email}).lean().exec(function(err, consumer) {
            if(err) throw err;
            if(consumer) {
              req.consumer = consumer;
              next();
            } else {
              console.log('User not found! Invalid token!');
              return res.status(403).json({
                err: 'Failed to authenticate token.'
              });
            }
          });
        }); else return res.status(403).json({
          err: 'Failed to authenticate token.'
        });
      }
      else {
        console.log('Invalid token | route ->', req.url, ' | token given ->', token);
        return res.status(403).json({
          err: 'Failed to authenticate token.'
        });
      }
    });

  }
  else {

    // if there is no token
    // return an error
    console.log('No token!');
    return res.status(403).json({
      err: 'No token provided.'
    });

  }
});

router.post('/contact', function(req, res) {
  var contactData = modules.validator.isEmail(req.body.email) && req.body.full_name && req.body.text ? {
    email: req.body.email.toLowerCase(), full_name: req.body.full_name, text: req.body.text
  } : undefined;
  if(contactData) {
    var contact = new modules.Contact(contactData);
    contact.save(function(err) {
      if(err) throw err;
      res.json(contact.toObject());
    });
  } else res.status(400).end("Invalid post data!");
});

router.get('/subscribe', auth, function(req, res) {
  modules.Subscriber.find().lean().exec(function(err, subscribers) {
    if(err) throw err;
    res.json(subscribers);
  });
});

router.get('/contact', auth, function(req, res) {
  modules.Contact.find().lean().exec(function(err, contacts) { 
    if(err) throw err;
    res.end(JSON.stringify(contacts));
  });
});

router.post('/login', api.login);
router.get('/logout', api.logout);

router.post('/consumer', api.postConsumer);
router.get('/consumer', api.getConsumer);
router.get('/consumer/:consumer_id', api.getOneConsumer);
router.put('/consumer/:consumer_id', api.putConsumer);
router.delete('/consumer/:consumer_id', api.deleteConsumer);
router.get('/consumer/:consumer_id/problem', api.getConsumerProblem); //get all problem if owner otherwise only valid same with provider

router.post('/provider', api.postProvider);
router.get('/provider', api.getProvider);
router.get('/provider/:provider_id', api.getOneProvider);
router.put('/provider/:provider_id', api.putProvider);
router.delete('/provider/:provider_id', api.deleteProvider);
router.get('/provider/:provider_id/solution', api.getProviderSolution);
router.get('/provider/:provider_id/review', api.getProviderReview);
router.get('/provider/:provider_id/comment', api.getProviderComment);
router.put('/provider/:provider_id/comment', api.approveDisapproveComment);

router.post('/problem', api.postProblem);
router.get('/problem', api.getProblem);
router.get('/problem/:problem_id', api.getOneProblem);
router.put('/problem/:problem_id', api.putProblem);
router.delete('/problem/:problem_id', api.deleteProblem);

router.post('/solution', api.postSolution);
router.get('/solution', api.getSolution);
router.get('/solution/:solution_id', api.getOneSolution);
router.put('/solution/:solution_id', api.putSolution);
router.delete('/solution/:solution_id', api.deleteSolution);
router.post('/solution/:solution_id/deal', api.makeDeal);

router.post('/bid', api.postBid);
router.get('/bid', api.getBid);
router.get('/bid/:bid_id', api.getOneBid);
router.put('/bid/:bid_id', api.acceptRejectBid);
router.delete('/bid/:bid_id', api.deleteBid);

router.get('/deal', api.getDeal);
router.get('/deal/:deal_id', api.getOneDeal);
router.put('/deal/:deal_id', api.putDeal);
router.post('/deal/:deal_id/review', api.postDealReview);
router.post('/deal/:deal_id/comment', api.postDealComment);

var http_port = 8008;
http_server.listen(http_port, function() {
  console.log("Https Shifter Server listening at port " + http_port);
});