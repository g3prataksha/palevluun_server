var modules = require('./modules');

var consumer_api = {};

consumer_api.post = function(req, res) {
    var consumerObj = req.body.consumer;
    if(consumerObj.email && consumerObj.first_name && consumerObj.last_name && consumerObj.password) {
        modules.Consumer.findOne({email: consumerObj.email}, function(err, consumer) {
            if(err) throw err;
            if(consumer) modules.sendError(res, {err: "Duplicate email! Account with this email already exist."}, 400);
            else {
                consumer =  new modules.Consumer(consumerObj);
                consumer.save(function(err) {
                    if(err) throw err;
                    consumer = consumer.toObject();
                    var credential = new modules.Credential({consumer: consumer._id});
                    modules.bcrypt.genSalt(10, function(err, salt) {
                        if (err) throw err;
                        modules.bcrypt.hash(consumerObj.password, salt, null, function(err, hash) {
                            if (err) throw err;
                            credential.password = hash;
                            credential.save(function(err) {
                                if(err) throw err;
                                modules.sendResponse(res, consumer);
                            });
                        });
                    });
                });
            }
        });
    } else modules.sendError(res, {err: "Bad request!"}, 400);
};

consumer_api.get = function(req, res) {
    console.log("consumer get api run test!");
    modules.Consumer.find({}).lean().exec(function(err, consumers) {
        if(err) throw err;
        modules.sendResponse(res, consumers);
    });
};

consumer_api.getOne = function(req, res) {
    var consumer_id = +req.params.consumer_id;
    if(req.consumer) {
        if(req.consumer._id === consumer_id) modules.Consumer.findById(consumer_id).lean().exec(function(err, consumer) {
            if(err) throw err;
            if(consumer) modules.sendResponse(res, consumer);
            else modules.sendError(res, {err: "Consumer not found"}, 404);
        }); else modules.sendError(res, {err: "Not allowed"}, 405);
    } else modules.sendError(res, {err: "Bad request"}, 400); 
};

consumer_api.put = function(req, res) {
    var consumer_id = +req.params.consumer_id, consumerData = req.body.consumer, valid = false;
    for(var key in consumerData) {
        if(consumerData[key]) valid = true;
        if(key === "email") {
            if(!modules.validator.isEmail(consumerData[key])) valid = false;
        }
    }
    if(req.consumer && valid) {
        if(consumerData.email) modules.Consumer.findOne({email: consumerData.email}).lean().exec(function(err, consumer) {
            if(err) throw err;
            if(consumer) modules.sendError(res, {err: "Duplicate email"}, 400);
            else update();
        }); else update();
        
        function update() {
            if(req.consumer._id === consumer_id) modules.Consumer.findById(consumer_id).exec(function(err, consumer) {
                if(err) throw err;
                if(consumer) {
                    for(var key in consumerData) {
                        if(consumerData[key]) consumer[key] = consumerData[key];
                    }
                    consumer.save(function(err) {
                        if(err) throw err;
                        modules.sendResponse(res, consumer.toObject());
                    });
                } else modules.sendError(res, {err: "Consumer not found"}, 404);
            }); else modules.sendError(res, {err: "Not allowed"}, 405);
        }
    } else modules.sendError(res, {err: "Bad request"}, 400); 
};

consumer_api.delete = function(req, res) {
    var consumer_id = +req.params.consumer_id;
    if(req.consumer) {
        if(req.consumer._id !== consumer_id) consumer_id = undefined;
    } else consumer_id = undefined;
    if(consumer_id) modules.Consumer.remove({_id: consumer_id}, function(err) {
        if(err) throw err;
        modules.sendResponse(res, {status: 200})
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

consumer_api.getProblem = function(req, res) {
    var consumer_id = +req.params.consumer_id;
    if(req.consumer) {
        if(req.consumer._id === consumer_id) modules.Problem.find({consumer: consumer_id}).lean().exec(function(err, problems) {
            if(err) throw err;
            modules.sendResponse(res, problems);
        }); else modules.sendError(res, {err: "Not allowed"}, 405);
    } else modules.sendError(res, {err: "Not allowed"}, 405); 
};

module.exports = consumer_api;