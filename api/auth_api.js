var modules = require('./modules');

var auth_api = {};

var checkPassword = function(credential, password, cb) {
    if(credential) modules.bcrypt.compare(password, credential.password, function(err, result) {
        if(err) throw err;
        if(result) {
            cb(null, result);
            //var token = modules.jwt.sign({email: provider.email}, modules.secret, {expiresIn: '7 days'});
            //modules.sendResponse(res, {user: provider, token: token, expiry_date: (Date.now()+24*60*60*1000)});
        } else  cb({err: "Incorrect email/password"});
    }); else cb({err: "Incorrect email/password"});
}

auth_api.login = function(req, res) {
    var email = req.body.login.email, password = req.body.login.password;
    if(email && password) {
        modules.Provider.findOne({email: email.toLowerCase(), status: 1}).lean().exec(function(err, provider) {
            if(err) throw err;
            if(provider) modules.Credential.findOne({provider: provider._id, consumer: {$exists: false}}).lean().exec(function(err, credential) {
                if(err) throw err;
                if(credential) checkPassword(credential, password, function(err, result) {
                    if(err) modules.sendError(res, err, 403);
                    else {
                        var token = modules.jwt.sign({email: provider.email}, modules.secret, {expiresIn: '7 days'});
                        res.cookie("token", token, {
            				signed: true
            			});
                        modules.sendResponse(res, {provider: provider, token: token, expiry_date: (Date.now()+24*60*60*1000)});
                    }
                }); else modules.sendError(res, {err: "Login failed"}, 403);
            }); else modules.Consumer.findOne({email: email.toLowerCase(), status: 1}).lean().exec(function(err, consumer) {
                if(err) throw err;
                if(consumer) modules.Credential.findOne({consumer: consumer._id, provider: {$exists: false}}).lean().exec(function(err, credential) {
                    if(err) throw err;
                    if(credential) checkPassword(credential, password, function(err, result) {
                        if(err) modules.sendError(res, err, 403);
                        else {
                            var token = modules.jwt.sign({email: consumer.email}, modules.secret, {expiresIn: '7 days'});
                            res.cookie("token", token, {
            			    	signed: true
            			    });
                            modules.sendResponse(res, {consumer: consumer, token: token, expiry_date: (Date.now()+24*60*60*1000)});
                        }
                    }); else modules.sendError(res, {err: "Login failed"}, 403);
                }); else modules.sendError(res, {err: "User not found!"}, 404);
            });
        });
    } else modules.sendError(res, {err: "Bad request! Invalid data!"}, 400);
};

auth_api.logout = function(req, res) {
    res.clearCookie("token");
    modules.sendResponse(res, {status: 200});
};

module.exports = auth_api;