var modules = require('./modules');

var problem_api = {};

problem_api.post = function(req, res) {
    if(modules.isObjValid(req.body.problem) && req.consumer) {
        var problemObj = req.body.problem;
        problemObj.consumer = req.consumer._id;
        var problem = new modules.Problem(problemObj);
        problem.save(function(err) {
            if(err) throw err;
            modules.sendResponse(res, problem.toObject());
        });
    } else modules.sendError(res, {err: "Not allowed"}, 405);
};

problem_api.get = function(req, res) {
    var last_id = +req.query.last_id, date_now = new Date(),
        query = last_id ? {_id: {$lt: last_id}, last_date: {$gt: date_now}, status: "open"} : {last_date: {$gt: date_now}, status: "open"};
    modules.Problem.find(query).sort({_id: -1}).limit(9).lean().exec(function(err, problems) {
        if(err) throw err;
        if(problems.length) modules.Problem.count({_id: {$gt: problems[problems.length-1]._id}}, function(err, count) {
            if(err) throw err;
            var json = {problems: problems, loadmore: count};
            modules.sendResponse(res, json);
        }); else modules.sendResponse(res, problems);
    });
};

problem_api.getOne = function(req, res) {
    var problem_id = +req.params.problem_id;
    if(problem_id && (req.consumer ||req.provider)) modules.Problem.findById(problem_id).populate({path: "consumer", select: "first_name last_name"}).lean().exec(function(err, problem) {
        if(err) throw err;
        if(problem) modules.sendResponse(res, problem);
        else modules.sendError(res, {err: "Problem not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

problem_api.put = function(req, res) {
    var problem_id = +req.params.problem_id;
     console.log("Problem:", JSON.stringify(req.body.problem));
    var valid = false;
    for (var key in req.body.problem) {
        if(req.body.problem[key]) valid = true;
    }
    if(problem_id && req.consumer && valid) modules.Problem.findOne({_id: problem_id, consumer: req.consumer._id}).exec(function(err, problem) {
        if(err) throw err;
        if(problem) {
            for (var key in problem) {
                if(req.body.problem[key]) problem[key] = req.body.problem[key];
            }
            problem.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, problem.toObject());
            });
        } else modules.sendError(res, {err: "Problem not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

problem_api.delete = function(req, res) {
    var problem_id = +req.params.problem_id;
    if(problem_id && req.consumer) modules.Problem.findOne({_id: problem_id, consumer: req.consumer._id}, function(err, problem) {
        if(err) throw err;
        if(problem) problem.remove(function(err) {
            if(err) throw err;
            if(problem.image) modules.deleteImage(problem.image, function(err) {
                if(err) throw err;
                modules.sendResponse(res, {"status": 200});
            }); else modules.sendResponse(res, {"status": 200}); 
        }); else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

problem_api.getImage = function(req, res) {
    var problem_id = +req.params.problem_id, image_id = modules.objectIdRegex.match(req.params.image_id) ? req.parans.image_id : undefined;
    if(problem_id && image_id) modules.Problem.findOne({_id: problem_id, image: image_id}).select('image').lean().exec(function(err, problem) {
        if(err) throw err;
        if(problem) modules.sendImage(res, problem.image);
        else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

problem_api.postImage = function(req, res) {
    var problem_id = +req.params.problem_id;
    if(problem_id && req.consumer && req.file) modules.Problem.findOne({_id: problem_id, consumer: req.consumer._id}, function(err, problem) {
        if(err) throw err;
        if(problem) modules.uploadImage({old_image: problem.image, file: req.file}, function(err, image) {
            if(err) throw err;
            problem.image = image;
            problem.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, problem.toObject());
            });
        }); else {
            modules.deleteFile(req.file);
            modules.sendError(res, {err: "Resource not found"}, 404);
        }
    }); else {
        modules.deleteFile(req.file);
        modules.sendError(res, {err: "Bad request"}, 400);
    }
};

problem_api.delImage = function(req, res) {
    var problem_id = +req.params.problem_id, image_id = modules.objectIdRegex.match(req.params.image_id) ? req.parans.image_id : undefined;
    if(problem_id && image_id && req.consumer) modules.Problem.findOne({_id: problem_id, consumer: req.consumer._id, image: image_id}).select('image').lean().exec(function(err, problem) {
        if(err) throw err;
        if(problem) {
            if(problem.image) modules.deleteImage(problem.image, function(err) {
                if(err) throw err;
                modules.sendResponse(res, {"status": 200});
            });
        } else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

module.exports = problem_api;