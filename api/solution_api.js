var modules = require('./modules');

var solution_api = {};

//missing most popular solution api query
solution_api.get = function(req, res) {
    var last_id = +req.query.last_id,
        query = last_id ? {_id: {$lt: last_id}, last_date: {$gt: Date.now()}} : {last_date: {$gt: Date.now()}};
    
    if(req.query.type === "top_offer") modules.Solution.find({type: "top_offer"}).sort({_id: -1}).limit(5).lean().exec(function(err, solutions) {
        if(err) throw err;
        modules.sendResponse(res, solutions);
    }); else modules.Solution.find(query).sort({_id: -1}).limit(9).lean().exec(function(err, solutions) {
        if(err) throw err;
        if(solutions.length) modules.Solution.count({_id: {$gt: solutions[solutions.length-1]._id}}, function(err, count) {
            if(err) throw err;
            var json = {solutions: solutions, loadmore: count};
            modules.sendResponse(res, json);
        }); else modules.sendResponse(res, solutions);
    });
};

solution_api.getOne = function(req, res) {
    var solution_id = +req.params.solution_id;
    if(solution_id) modules.Solution.findById(solution_id).lean().exec(function(err, solution) {
        if(err) throw err;
        if(solution) modules.sendResponse(res, solution);
        else modules.sendError(res, {err: "Solution not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

solution_api.post = function(req, res) {
    if(modules.isObjValid(req.body.solution, ["type"]) && req.provider) {
        var solutionObj = req.body.solution;
        solutionObj.provider = req.provider._id;
        var solution = new modules.Solution(solutionObj);
        solution.save(function(err) {
            if(err) throw err;
            modules.sendResponse(res, solution.toObject());
        });
    } else modules.sendError(res, {err: "Not allowed"}, 405);
};

solution_api.put = function(req, res) {
    var solution_id = +req.params.solution_id;
    var valid = false;
    for (var key in req.body.solution) {
        if(req.body.solution[key]) valid = true;
    }
    if(solution_id && req.provider && valid) modules.Solution.findOne({_id: solution_id, provider: req.provider._id}).exec(function(err, solution) {
        if(err) throw err;
        if(solution) {
            for (var key in solution) {
                if(req.body.solution[key]) solution[key] = req.body.solution[key];
            }
            solution.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, solution.toObject());
            });
        } else modules.sendError(res, {err: "solution not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

solution_api.delete = function(req, res) {
    var solution_id = +req.params.solution_id;
    if(solution_id && req.provider) modules.Solution.findOne({_id: solution_id, provider: req.provider._id}, function(err, solution) {
        if(err) throw err;
        if(solution) solution.remove(function(err) {
            if(err) throw err;
            if(solution.image) modules.deleteImage(solution.image, function(err) {
                if(err) throw err;
                modules.sendResponse(res, {"status": 200});
            }); else modules.sendResponse(res, {"status": 200});
        }); else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

solution_api.postImage = function(req, res) {
    var solution_id = +req.params.solution_id;
    if(solution_id && req.provider && req.file) modules.Solution.findOne({_id: solution_id, provider: req.provider._id}, function(err, solution) {
        if(err) throw err;
        if(solution) modules.uploadImage({old_image: solution.image, file: req.file}, function(err, image) {
            if(err) throw err;
            solution.image = image;
            solution.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, solution.toObject());
            });
        }); else {
            modules.deleteFile(req.file);
            modules.sendError(res, {err: "Resource not found"}, 404);
        }
    }); else {
        modules.deleteFile(req.file);
        modules.sendError(res, {err: "Bad request"}, 400);
    }
};

solution_api.getImage = function(req, res) {
    var solution_id = +req.params.solution_id, image_id = modules.objectIdRegex.match(req.params.image_id) ? req.parans.image_id : undefined;
    if(solution_id && image_id) modules.Solution.findOne({_id: solution_id, image: image_id}).select('image').lean().exec(function(err, solution) {
        if(err) throw err;
        if(solution) modules.sendImage(res, solution.image);
        else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

solution_api.delImage = function(req, res) {
    var solution_id = +req.params.solution_id, image_id = modules.objectIdRegex.match(req.params.image_id) ? req.parans.image_id : undefined;
    if(solution_id && image_id && req.provider) modules.Solution.findOne({_id: solution_id, provider: req.provider._id, image: image_id}).select('image').lean().exec(function(err, solution) {
        if(err) throw err;
        if(solution) {
            if(solution.image) modules.deleteImage(solution.image, function(err) {
                if(err) throw err;
                modules.sendResponse(res, {"status": 200});
            });
        } else modules.sendError(res, {err: "Resource not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

solution_api.makeDeal = function(req, res) {
    var solution_id = +req.params.solution_id;
    if(solution_id && req.consumer) modules.Solution.findOne({_id: solution_id, consumer: req.consumer._id}, function(err, solution) {
        if(err) throw err;
        //create new deal
        var deal = new modules.Deal({solution: solution._id, consumer: req.consumer._id, provider: solution.provider, created_by: "provider", uuid: modules.uuid.v4()})
        deal.save(function(err) {
            if(err) throw err;
            modules.sendResponse(res, deal.toObject());
        });
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

module.exports = solution_api;