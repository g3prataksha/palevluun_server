var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var imageSchema = new Schema({
	key: String
});

module.exports = mongoose.model('image', imageSchema);