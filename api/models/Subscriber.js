var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var subscriberSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	email: String
});

subscriberSchema.plugin(autoIncrement.plugin, {model: 'subscriber', startAt: 1});
var subscriber = mongoose.model('subscriber', subscriberSchema);

module.exports = subscriber;