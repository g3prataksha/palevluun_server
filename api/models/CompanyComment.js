var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var companyCommentSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	text: String,
	consumer: {
	    type: Number, ref: 'consumer'
	},
	provider: {
	    type: Number, ref: 'provider'
	}
});

companyCommentSchema.plugin(autoIncrement.plugin, {model: 'companyComment', startAt: 1});
var companyComment = mongoose.model('companyComment', companyCommentSchema);

companyCommentSchema.pre("save", function(next) {
    this.updated = new Date();
    next();
});

module.exports = companyComment;