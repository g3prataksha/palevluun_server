var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var companyReviewSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	ratings: Number,
	consumer: {
	    type: Number, ref: 'consumer'
	},
	provider: {
	    type: Number, ref: 'provider'
	}
});

companyReviewSchema.plugin(autoIncrement.plugin, {model: 'companyReview', startAt: 1});
var companyReview = mongoose.model('companyReview', companyReviewSchema);

companyReviewSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = companyReview;