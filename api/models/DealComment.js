var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var dealCommentSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	text: String,
	consumer: {
	    type: Number, ref: 'consumer'
	},
	deal: {
	    type: Number, ref: 'deal'
	},
	status: {type: String, default: 'pending'}
});

dealCommentSchema.plugin(autoIncrement.plugin, {model: 'dealComment', startAt: 1});
var dealComment = mongoose.model('dealComment', dealCommentSchema);

dealCommentSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = dealComment;