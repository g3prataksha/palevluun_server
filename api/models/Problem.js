var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var problemSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	title: [{
		locale: String, text: String
	}],
	description:  [{
		locale: String, text: String
	}],
	prefered_location: String, //around 10 km e.g.
	brand: String,
	equipment_info: String,
	image: String,
	sector: {
	    type: Number,
	    ref: 'sector'
	},
	subSector: {
	    type: Number,
	    ref: 'subSector'
	},
	consumer: {
	    type: Number,
	    ref: 'consumer'
	},
	price_range: {
	    start: Number, end: Number, currency: String  
	},
	last_date: Date,
	status: {type: String, default: "open"}
});

problemSchema.plugin(autoIncrement.plugin, {model: 'problem', startAt: 1});
var problem = mongoose.model('problem', problemSchema);

problemSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = problem;