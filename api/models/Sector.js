var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var sectorSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	name: [{
	    locale: String, text: String
	}]
});

var sector = mongoose.model('sector', sectorSchema);

sectorSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = sector;