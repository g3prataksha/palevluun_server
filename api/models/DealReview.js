var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var dealReviewSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	ratings: Number,
	consumer: {
	    type: Number, ref: 'consumer'
	},
	deal: {
	    type: Number, ref: 'deal'
	}
});

dealReviewSchema.plugin(autoIncrement.plugin, {model: 'dealReview', startAt: 1});
var dealReview = mongoose.model('dealReview', dealReviewSchema);

dealReviewSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = dealReview;