var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var credentialSchema = new Schema({
	password: String,
	created: {type: Date, default: Date.now},
	updated: {type: Date, default: Date.now},
	consumer: {
		type: Number,
		ref: 'user'
	},
	provider: {
		type: Number,
		ref: 'user'
	}
});

module.exports = mongoose.model('credential', credentialSchema);