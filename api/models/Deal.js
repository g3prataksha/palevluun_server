var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');
/* Bid + problem || solution + consumer*/
var dealSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	bid: {
		type: Number,
		ref: 'bid'
	},
	problem: {
		type: Number,
		ref: 'problem'
	},
	consumer: {
		type: Number,
		ref: 'consumer'
	},
	provider: {
		type: Number,
		ref: 'provider'
	},
	solution: {
		type: Number,
		ref: 'solution'
	},
	uuid: String,
	created_by: {type: String, enum: ["provider", "consumer"]},
	status: {type: String, default: 'unconfirmed'}
});

dealSchema.plugin(autoIncrement.plugin, {model: 'deal', startAt: 1});
var deal = mongoose.model('deal', dealSchema);

dealSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = deal;