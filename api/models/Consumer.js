var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var consumerSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	first_name: String,
	last_name: String,
	email: {type: String, unique: true, required: true},
	phone: String,
	city: String,
	postal: String,
	address: String,
	country: String,
	preferable_contact_media: String,
	preferable_time: String,
	avatar: String,
	status: {type: Number, default: 1}
});

consumerSchema.plugin(autoIncrement.plugin, {model: 'consumer', startAt: 1});
var consumer = mongoose.model('consumer', consumerSchema);

consumerSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

consumerSchema.post("remove", function(doc) {	
    var modules = require('./../modules');
	if(doc.avatar) modules.Image.remove({_id: doc.avatar}).exec();
	modules.Credential.remove({consumer: doc._id}).exec();
});

module.exports = consumer;