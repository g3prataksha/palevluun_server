var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var solutionSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	title: [{
		locale: String, text: String
	}],
    provider: {
        type: Number,
        ref: 'provider'
    },
	description: [{
		locale: String, text: String
	}],
	special_offer: [{
		locale: String, text: String
	}],
	sector: {
	    type: Number,
	    ref: 'sector'
	},
	subSector: {
	    type: Number,
	    ref: 'subSector'
	},
	price_range: {
	    start: Number, end: Number, cuurency: String  
	},
	last_date: Date,
	type: String
});

solutionSchema.plugin(autoIncrement.plugin, {model: 'solution', startAt: 1});
var solution = mongoose.model('solution', solutionSchema);

solutionSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = solution;