var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var bidSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	problem: {
	    type: Number,
	    ref: 'problem'
	},
	provider: {
	    type: Number,
	    ref: 'provider'
	},
	price_range: {
	    start: Number, end: Number, currency: String 
	},
	status: {type: String, default: "pending"},
	last_date: Date
});

bidSchema.plugin(autoIncrement.plugin, {model: 'bid', startAt: 1});
var bid = mongoose.model('bid', bidSchema);

bidSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = bid;