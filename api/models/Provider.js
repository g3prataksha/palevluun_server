var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var providerSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	name: String,
	registration_number: String,
	contact_person: {
	    first_name: String,
	    last_name: String,
	    email: String,
	    phone: String
	},
	email: {type: String, unique: true, required: true},
	phone: String,
	country: String,
	city: String,
	postal: String,
	address: String,
	preferable_contact_media: String,
	preferable_time: String,
	avatar: String,
	opening_hours: [{
	    day: String,
	    hours: String
	}],
	total_employees: Number,
	sector: {
	    type: Number, ref: 'sector'
	},
	sub_sector: {
	    type: Number, ref: 'subSector'
	},
	status: {type: Number, default: 1}
});

providerSchema.plugin(autoIncrement.plugin, {model: 'provider', startAt: 1});
var provider = mongoose.model('provider', providerSchema);

providerSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

providerSchema.post("remove", function(doc) {	
    var modules = require('./../modules');
	if(doc.avatar) modules.Image.remove({_id: doc.avatar}).exec();
	modules.Credential.remove({provider: doc._id}).exec();
});

module.exports = provider;