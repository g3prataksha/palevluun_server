var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var subSectorSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	name: [{
	    locale: String, text: String
	}],
	sector: {
	    type: Number,
	    ref: 'sector'
	}
});

var subSector = mongoose.model('subSector', subSectorSchema);

subSectorSchema.pre("save", function(next) {
    this.updated = Date.now();
    next();
});

module.exports = subSector;