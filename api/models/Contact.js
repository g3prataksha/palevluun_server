var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	autoIncrement = require('mongoose-auto-increment');

var contactSchema = new Schema({
	created: {type: Date, default: Date.now},
	updated: Date,
	email: String,
	full_name: String,
	text: String
});

contactSchema.plugin(autoIncrement.plugin, {model: 'contact', startAt: 1});
var contact = mongoose.model('contact', contactSchema);

module.exports = contact;