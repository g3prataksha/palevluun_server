var modules = require('./modules');

var deal_api = {};

//deal status must not be deleted
deal_api.get = function(req, res) {
    var query, status = (req.query.status === "cancel" || req.query.status === "open" || req.query.status === "working" || req.query.status === "ready" || req.query.status === "extended") ? req.query.status : undefined;;
    if(req.consumer) query = {consumer: req.consumer._id, status: status};
    else if(req.provider) query = {provider: req.provider._id, status: status};
    if(query) modules.Deal.find(query).populate([{path: "provider", select: "name avatar"}, {path: "consumer", select: "first_name last_name avatar"}]).lean().exec(function(err, deals) {
        if(err) throw err;
        modules.sendResponse(res, deals);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

deal_api.getOne = function(req, res) {
    var query, deal_id = +req.params.deal_id;
    if(req.consumer) query = {consumer: req.consumer._id, _id: deal_id};
    if(req.provider) query = {provider: req.provider._id, _id: deal_id};
    if(query && deal_id) modules.Deal.findOne(query).populate([{path: "provider", select: "name avatar email contact_person phone"}, {path: "consumer", select: "first_name last_name avatar email phone"}]).lean().exec(function(err, deal) {
        if(err) throw err;
        if(deal) modules.sendResponse(res, deal);
        else modules.sendError(res, {err: "Deal not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

deal_api.put = function(req, res) {
    var query, deal_id = +req.params.deal_id, status = (req.body.deal.status === "canceled" || req.body.deal.status === "open" || req.body.deal.status === "working" || req.body.deal.status === "ready" || req.body.deal.status === "extended") ? req.body.deal.status : undefined;
    if(req.provider) query = {provider: req.provider._id, _id: deal_id};
    if(query && deal_id && req.body.deal) modules.Deal.findOne(query, function(err, deal) {
        if(err) throw err;
        if(deal) {
            deal.status = status;
            deal.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, deal.toObject());
            });
        } else modules.sendError(res, {err: "Deal not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

deal_api.postComment = function(req, res) {
    var deal_id = +req.params.deal_id,
        text = req.body.deal.text ? modules.sanitizeHtml(req.body.deal.text) : undefined;
    if(req.consumer && deal_id && text) modules.Deal.findOne({consumer: req.consumer._id, _id: deal_id}).lean().exec(function(err, deal) {
        if(err) throw err;
        if(deal) {
            var comment = new modules.DealComment({consumer: req.consumer._id, deal: deal._id, text: text});
            comment.save(function(err) {
                if(err) throw err;
                modules.sendResponse(res, comment.toObject());
            });
        } else modules.sendError(res, {err: "Deal not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

deal_api.postReview = function(req, res) {
    var deal_id = +req.params.deal_id,
        ratings = +req.body.deal.ratings;
    if(req.consumer && deal_id && ratings >= 0) modules.Deal.findOne({consumer: req.consumer._id, _id: deal_id}).lean().exec(function(err, deal) {
        if(err) throw err;
        if(deal) {
            modules.DealReview.findOne({consumer: req.consumer._id, deal: deal._id}, function(err, comment) {
                if(err) throw err;
                if(comment) comment.ratings = ratings;
                else comment = new modules.DealReview({consumer: req.consumer._id, deal: deal._id, ratings: ratings});
                comment.save(function(err) {
                    if(err) throw err;
                    modules.sendResponse(res, comment.toObject());
                });
            });
        } else modules.sendError(res, {err: "Deal not found"}, 404);
    }); else modules.sendError(res, {err: "Bad request"}, 400);
};

module.exports = deal_api;